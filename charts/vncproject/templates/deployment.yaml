apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "vncproject.fullname" . }}
  labels:
    {{- include "vncproject.labels" . | nindent 4 }}
spec:
{{- if not .Values.autoscaling.enabled }}
  replicas: {{ .Values.replicaCount }}
{{- end }}
  selector:
    matchLabels:
      app: vncproject
      {{- include "vncproject.selectorLabels" . | nindent 6 }}
  template:
    metadata:
    {{- with .Values.podAnnotations }}
      annotations:
        {{- toYaml . | nindent 8 }}
    {{- end }}
      labels:
        app: vncproject
        {{- include "vncproject.selectorLabels" . | nindent 8 }}
    spec:
      {{- with .Values.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      serviceAccountName: {{ include "vncproject.serviceAccountName" . }}
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}
      dnsPolicy: {{ .Values.dnsPolicy }}
      affinity:
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
          - weight: 100
            podAffinityTerm:
              labelSelector:
                matchExpressions:
                - key: app
                  operator: In
                  values:
                  - vncproject
              topologyKey: "kubernetes.io/hostname"

      {{- if not .Values.env.DisableInitContainers }}
      initContainers:
        - name: init-vnclagoon-docs
          image: {{ .Values.docs.image }}
          command: ['sh', '-c', "cp -r -f /files/. /opt/vncproject/files/documentation/"]
          volumeMounts:
            - name: files
              mountPath: /opt/vncproject/files

      initContainers:
        - name: db-migrations
          image: "{{ .Values.image.repository }}:{{ .Values.image.tag | default .Chart.AppVersion }}"
          command: ['sh', '-c', "bundle exec rake redmine:install RAILS_ENV=production && bundle exec rake redmine:plugins:migrate RAILS_ENV=production"]
          env:
          {{- range $key, $value := $.Values.env }}
          - name: {{ $key }}
            value: {{ $value | quote }}
          {{- end }}
      {{- end }}

      containers:
        - name: {{ .Chart.Name }}
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ .Values.image.repository }}:{{ .Values.image.tag | default .Chart.AppVersion }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          ports:
            - name: http
              containerPort: 3000
              protocol: TCP
          livenessProbe:
            httpGet:
              path: /healthcheck
              port: 3000
            initialDelaySeconds: 60
            timeoutSeconds: 20
          readinessProbe:
            httpGet:
              path: /healthcheck
              port: 3000
            initialDelaySeconds: 60
            timeoutSeconds: 20
          volumeMounts:
            - name: files
              mountPath: /opt/vncproject/files
            {{- if .Values.env.UseConnectionString }}
            - name: project-pgpass-config
              mountPath: /root/.pgpass
              subPath: .pgpass
            {{- end }}
            {{- if .Values.saas.enabled }}
            - name: project-saas-config-file
              mountPath: /opt/vncproject
            {{- end }}
          env:
          {{- range $key, $value := $.Values.env }}
          - name: {{ $key }}
            value: {{ $value | quote }}
          {{- end }}
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
        - name: sidekiq
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ .Values.sidekiq.image.repository }}:{{ .Values.sidekiq.image.tag | default .Chart.AppVersion }}"
          imagePullPolicy: {{ .Values.sidekiq.image.pullPolicy }}
          command: ["/bin/sh"]
          args: ["-c", "bundle exec sidekiq -e production"]
          livenessProbe:
            exec:
              command:
              - cat
              - /etc/hosts
            initialDelaySeconds: 60
          spec:
          env:
          {{- range $key, $value := $.Values.env }}
          - name: {{ $key }}
            value: {{ $value | quote }}
          {{- end }}
          volumeMounts:
            - name: files
              mountPath: /opt/vncproject/files
            {{- if .Values.saas.enabled }}
            - name: project-saas-config-file
              mountPath: /opt/vncproject
            {{- end }}
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
      {{- with .Values.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      volumes:
        - name: files
          persistentVolumeClaim:
            claimName: {{ include "vncproject.fullname" . }}
        {{- if .Values.env.UseConnectionString }}
        - name: project-pgpass-config
          configMap:
            name: project-pgpass-config
        {{- end }}
        {{- if .Values.saas.enabled }}
        - name: project-saas-config-file
          configMap:
            name: project-saas-config-file
        {{- end }}
