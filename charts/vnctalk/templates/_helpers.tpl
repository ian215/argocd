{{/*
Expand the name of the chart.
*/}}
{{- define "vnctalk.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "vnctalk.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "vnctalk.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "vnctalk.labels" -}}
helm.sh/chart: {{ include "vnctalk.chart" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "vnctalkuxf.selectorLabels" -}}
app.kubernetes.io/name: {{ "vnctalk-uxf" }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{- define "vnctalksolr.selectorLabels" -}}
app.kubernetes.io/name: {{ "vnctalk-solr" }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{- define "vnctalketherpad.selectorLabels" -}}
app.kubernetes.io/name: {{ "vnctalk-etherpad" }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{- define "vnctalkwhiteboard.selectorLabels" -}}
app.kubernetes.io/name: {{ "vnctalk-whiteboard" }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{- define "vnctalknotificationproxy.selectorLabels" -}}
app.kubernetes.io/name: {{ "vnctalk-notificationproxy" }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "vnctalk.uxfweb.serviceAccountName" -}}
{{- if .Values.uxfweb.serviceAccount.create }}
{{- default (include "vnctalk.fullname" .) .Values.uxfweb.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.uxfweb.serviceAccount.name }}
{{- end }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "vnctalk.uxfapi.serviceAccountName" -}}
{{- if .Values.uxfapi.serviceAccount.create }}
{{- default (include "vnctalk.fullname" .) .Values.uxfapi.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.uxfapi.serviceAccount.name }}
{{- end }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "vnctalk.solr.serviceAccountName" -}}
{{- if .Values.solr.serviceAccount.create }}
{{- default (include "vnctalk.fullname" .) .Values.solr.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.solr.serviceAccount.name }}
{{- end }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "vnctalk.etherpad.serviceAccountName" -}}
{{- if .Values.etherpad.serviceAccount.create }}
{{- default (include "vnctalk.fullname" .) .Values.etherpad.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.etherpad.serviceAccount.name }}
{{- end }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "vnctalk.xmpp.serviceAccountName" -}}
{{- if .Values.xmpp.serviceAccount.create }}
{{- default (include "vnctalk.fullname" .) .Values.xmpp.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.xmpp.serviceAccount.name }}
{{- end }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "vnctalk.notificationproxy.serviceAccountName" -}}
{{- if .Values.notificationproxy.serviceAccount.create }}
{{- default (include "vnctalk.fullname" .) .Values.notificationproxy.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.notificationproxy.serviceAccount.name }}
{{- end }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "vnctalk.whiteboard.serviceAccountName" -}}
{{- if .Values.whiteboard.serviceAccount.create }}
{{- default (include "vnctalk.fullname" .) .Values.whiteboard.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.whiteboard.serviceAccount.name }}
{{- end }}
{{- end }}
