apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "vncdirectory.fullname" . }}
  labels:
    {{- include "vncdirectory.labels" . | nindent 4 }}
spec:
{{- if not .Values.autoscaling.enabled }}
  replicas: {{ .Values.replicaCount }}
{{- end }}
  selector:
    matchLabels:
      app: vncdirectory
  template:
    metadata:
    {{- with .Values.podAnnotations }}
      annotations:
        {{- toYaml . | nindent 8 }}
    {{- end }}
      labels:
        app: vncdirectory
        {{- include "vncdirectory.selectorLabels" . | nindent 8 }}
    spec:
      {{- with .Values.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      serviceAccountName: {{ include "vncdirectory.serviceAccountName" . }}
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}

      initContainers:
        - name: db-migrations
          image: "{{ .Values.image.repository }}:{{ .Values.image.tag | default .Chart.AppVersion }}"
          command: ['sh', '-c', "bundle exec rake redmine:install RAILS_ENV=production && bundle exec rake redmine:plugins:migrate RAILS_ENV=production"]
          env:
          {{- range $key, $value := $.Values.env }}
          - name: {{ $key }}
            value: {{ $value | quote }}
          {{- end }}

      containers:
        - name: {{ .Chart.Name }}
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ .Values.image.repository }}:{{ .Values.image.tag | default .Chart.AppVersion }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          livenessProbe:
            httpGet:
              path: /
              port: 3000
            timeoutSeconds: 10
            initialDelaySeconds: 60
          readinessProbe:
            httpGet:
              path: /
              port: 3000
            timeoutSeconds: 10
            initialDelaySeconds: 60
          ports:
            - name: http
              containerPort: 3000
              protocol: TCP
          volumeMounts:
            - name: data
              mountPath: /data
            {{ if .Values.locales.en.enabled }}
            - name: en-locales
              mountPath: "/opt/vnclagoon/plugins/translations_and_email_templates_override/config/locales/en.yml"
              subPath: "en.yml"
            {{ end }}
            {{ if .Values.locales.de.enabled }}
            - name: de-locales
              mountPath: "/opt/vnclagoon/plugins/translations_and_email_templates_override/config/locales/de.yml"
              subPath: "de.yml"
            {{ end }}
          spec:
          env:
          {{- range $key, $value := $.Values.env }}
          - name: {{ $key }}
            value: {{ $value | quote }}
          {{- end }}
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
        - name: sidekiq
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ .Values.sidekiq.image.repository }}:{{ .Values.image.tag | default .Chart.AppVersion }}"
          imagePullPolicy: {{ .Values.sidekiq.image.pullPolicy }}
          command: ["/bin/sh"]
          args: ["-c", "bundle exec sidekiq -e production"]
          livenessProbe:
            exec:
              command:
              - cat
              - /etc/hosts
            initialDelaySeconds: 60
          spec:
          env:
          {{- range $key, $value := $.Values.env }}
          - name: {{ $key }}
            value: {{ $value | quote }}
          {{- end }}
          volumeMounts:
            - name: data
              mountPath: /data
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
      {{- with .Values.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      volumes:
        - name: data
          persistentVolumeClaim:
            claimName: {{ include "vncdirectory.fullname" . }}
        {{ if .Values.locales.en.enabled }}
        - name: en-locales
          configMap:
            name: en-locales
        {{ end }}
        {{ if .Values.locales.de.enabled }}
        - name: de-locales
          configMap:
            name: de-locales
        {{ end }}
