#!/bin/bash
#
# create argo config for vnclagoon

ingressClass=$(crudini --get vnclagoon.cfg '' ingressClass | tr -d '"')
redis_storage_class=$(crudini --get vnclagoon.cfg '' redis-storage-class | tr -d '"')
solr_storage_class=$(crudini --get vnclagoon.cfg '' solr_storage_class | tr -d '"')
vnc_image_repository=$(crudini --get vnclagoon.cfg '' vnc-image-repository | tr -d '"')
repoURL=$(crudini --get vnclagoon.cfg '' repoURL | tr -d '"')
postgresql_host=$(crudini --get vnclagoon.cfg '' postgresql-host | tr -d '"')
etherpad_db_name=$(crudini --get vnclagoon.cfg '' etherpad_db_name | tr -d '"')
etherpad_db_pass=$(crudini --get vnclagoon.cfg '' etherpad_db_pass | tr -d '"')
etherpad_db_user=$(crudini --get vnclagoon.cfg '' etherpad_db_user | tr -d '"')
idp_db_name=$(crudini --get vnclagoon.cfg '' idp_db_name | tr -d '"')
idp_db_password=$(crudini --get vnclagoon.cfg '' idp_db_password | tr -d '"')
idp_db_username=$(crudini --get vnclagoon.cfg '' idp_db_username | tr -d '"')
prosody_db_name=$(crudini --get vnclagoon.cfg '' prosody_db_name | tr -d '"')
prosody_db_pass=$(crudini --get vnclagoon.cfg '' prosody_db_pass | tr -d '"')
prosody_db_user=$(crudini --get vnclagoon.cfg '' prosody_db_user | tr -d '"')
vncdirectory_db_name=$(crudini --get vnclagoon.cfg '' vncdirectory_db_name | tr -d '"')
vncdirectory_db_password=$(crudini --get vnclagoon.cfg '' vncdirectory_db_password | tr -d '"')
vncdirectory_db_username=$(crudini --get vnclagoon.cfg '' vncdirectory_db_username | tr -d '"')
vncproject_db_name=$(crudini --get vnclagoon.cfg '' vncproject_db_name | tr -d '"')
vncproject_db_password=$(crudini --get vnclagoon.cfg '' vncproject_db_password | tr -d '"')
vncproject_db_username=$(crudini --get vnclagoon.cfg '' vncproject_db_username | tr -d '"')
searchadmin_db_name=$(crudini --get vnclagoon.cfg '' searchadmin_db_name | tr -d '"')
searchadmin_db_user=$(crudini --get vnclagoon.cfg '' searchadmin_db_user | tr -d '"')
searchadmin_db_pass=$(crudini --get vnclagoon.cfg '' searchadmin_db_pass | tr -d '"')
nfs_server=$(crudini --get vnclagoon.cfg '' nfs-server | tr -d '"')
vncdirectory_nfs_path=$(crudini --get vnclagoon.cfg '' vncdirectory_nfs_path | tr -d '"')
avatar_nfs_path=$(crudini --get vnclagoon.cfg '' avatar-nfs-path | tr -d '"')
fileshare_nfs_path=$(crudini --get vnclagoon.cfg '' fileshare_nfs_path | tr -d '"')
vncproject_nfs_path=$(crudini --get vnclagoon.cfg '' vncproject_nfs_path | tr -d '"')
smtp_relay=$(crudini --get vnclagoon.cfg '' smtp_relay | tr -d '"')
avatar_tls_crt=$(crudini --get vnclagoon.cfg '' avatar-tls-crt | tr -d '"')
avatar_tls_key=$(crudini --get vnclagoon.cfg '' avatar-tls-key | tr -d '"')
idp_tls_crt=$(crudini --get vnclagoon.cfg '' idp-tls-crt | tr -d '"')
idp_tls_key=$(crudini --get vnclagoon.cfg '' idp-tls-key | tr -d '"')
talk_tls_crt=$(crudini --get vnclagoon.cfg '' talk-tls-crt | tr -d '"')
talk_tls_key=$(crudini --get vnclagoon.cfg '' talk-tls-key | tr -d '"')
etherpad_tls_crt=$(crudini --get vnclagoon.cfg '' etherpad-tls-crt | tr -d '"')
etherpad_tls_key=$(crudini --get vnclagoon.cfg '' etherpad-tls-key | tr -d '"')
vncdirectory_tls_crt=$(crudini --get vnclagoon.cfg '' vncdirectory-tls-crt | tr -d '"')
vncdirectory_tls_key=$(crudini --get vnclagoon.cfg '' vncdirectory-tls-key | tr -d '"')
vnc_fileshare_tls_crt=$(crudini --get vnclagoon.cfg '' vnc-fileshare-tls-crt | tr -d '"')
vnc_fileshare_tls_key=$(crudini --get vnclagoon.cfg '' vnc-fileshare-tls-key | tr -d '"')
vncproject_tls_crt=$(crudini --get vnclagoon.cfg '' vncproject-tls-crt | tr -d '"')
vncproject_tls_key=$(crudini --get vnclagoon.cfg '' vncproject-tls-key | tr -d '"')
vnctask_tls_crt=$(crudini --get vnclagoon.cfg '' vnctask-tls-crt | tr -d '"')
vnctask_tls_key=$(crudini --get vnclagoon.cfg '' vnctask-tls-key | tr -d '"')
xmpp_tls_crt=$(crudini --get vnclagoon.cfg '' xmpp-tls-crt | tr -d '"')
xmpp_tls_key=$(crudini --get vnclagoon.cfg '' xmpp-tls-key | tr -d '"')
vnclagoon_domain=$(crudini --get vnclagoon.cfg '' vnclagoon_domain | tr -d '"')
avatar_hostname=$(crudini --get vnclagoon.cfg '' avatar_hostname | tr -d '"')
etherpad_hostname=$(crudini --get vnclagoon.cfg '' etherpad_hostname | tr -d '"')
fileshare_hostname=$(crudini --get vnclagoon.cfg '' fileshare_hostname | tr -d '"')
idp_hostname=$(crudini --get vnclagoon.cfg '' idp_hostname | tr -d '"')
vncdirectory_hostname=$(crudini --get vnclagoon.cfg '' vncdirectory_hostname | tr -d '"')
vncproject_hostname=$(crudini --get vnclagoon.cfg '' vncproject_hostname | tr -d '"')
vnctalk_hostname=$(crudini --get vnclagoon.cfg '' vnctalk_hostname | tr -d '"')
vnctask_hostname=$(crudini --get vnclagoon.cfg '' vnctask_hostname | tr -d '"')
xmpp_hostname=$(crudini --get vnclagoon.cfg '' xmpp_hostname | tr -d '"')
xmpprest_hostname=$(crudini --get vnclagoon.cfg '' xmpprest_hostname | tr -d '"')
idp_admin_pass=$(crudini --get vnclagoon.cfg '' idp_admin_pass | tr -d '"')
vnctalk_admission_email=$(crudini --get vnclagoon.cfg '' vnctalk_admission_email | tr -d '"')
minscaling=$(crudini --get vnclagoon.cfg '' minscaling | tr -d '"')

export ingressClass
export redis_storage_class
export solr_storage_class
export vnc_image_repository
export repoURL
export postgresql_host
export etherpad_db_name
export etherpad_db_pass
export etherpad_db_user
export idp_db_name
export idp_db_password
export idp_db_username
export prosody_db_name
export prosody_db_pass
export prosody_db_user
export vncdirectory_db_name
export vncdirectory_db_password
export vncdirectory_db_username
export vncproject_db_name
export vncproject_db_password
export vncproject_db_username
export nfs_server
export vncdirectory_nfs_path
export avatar_nfs_path
export fileshare_nfs_path
export vncproject_nfs_path
export smtp_relay
export avatar_tls_crt
export avatar_tls_key
export idp_tls_crt
export idp_tls_key
export talk_tls_crt
export talk_tls_key
export etherpad_tls_crt
export etherpad_tls_key
export vncdirectory_tls_crt
export vncdirectory_tls_key
export vnc_fileshare_tls_crt
export vnc_fileshare_tls_key
export vncproject_tls_crt
export vncproject_tls_key
export vnctask_tls_crt
export vnctask_tls_key
export xmpp_tls_crt
export xmpp_tls_key
export vnclagoon_domain
export avatar_hostname
export etherpad_hostname
export fileshare_hostname
export idp_hostname
export vncdirectory_hostname
export vncproject_hostname
export vnctalk_hostname
export vnctask_hostname
export xmpp_hostname
export xmpprest_hostname
export idp_admin_pass
export vnctalk_admission_email
export searchadmin_db_name
export searchadmin_db_user
export searchadmin_db_pass

if [ -z "$minscaling" ]; then
	minscaling=1
fi
export minscaling

if [ -z "$redis_storage_class" ]; then
	echo "vnclagoon domain not set - bailing out"
	exit 1
fi

# shellcheck disable=SC2001
vncproject_domain_dcparts=$(echo "$vnclagoon_domain" | sed 's/\./,dc=/g')
export vncproject_domain_dcparts

if [ -z "$vncdirectory_tag_secret" ]; then
	vncdirectory_tag_secret=$(openssl rand -hex 40)
	export vncdirectory_tag_secret
fi

if [ -z "$vncdirectory_stat_secret" ]; then
	vncdirectory_stat_secret=$(openssl rand -hex 40)
	export vncdirectory_stat_secret
fi

if [ -z "$vncd_vncp_secretkeybase" ]; then
	vncd_vncp_secretkeybase=$(openssl rand -hex 40)
	export vncd_vncp_secretkeybase
fi


if [ -z "$etherpad_api_key" ]; then
	etherpad_api_key=$(openssl rand -hex 40)
	export etherpad_api_key
fi

if [ -z "$fileshare_secret" ]; then
	fileshare_secret=$(openssl rand -hex 40)
	export fileshare_secret
fi

if [ -z "$idp_salt" ]; then
	idp_salt=$(openssl rand -hex 20)
	export idp_salt
fi

if [ -z "$jwt_secret" ]; then
	jwt_secret=$(openssl rand -hex 40)
	export jwt_secret
fi

if [ -z "$vnctalk_auth" ]; then
	vnctalk_auth=$(echo -n "$jwt_secret" | jwt -claim email="admin@$vnclagoon_domain" -claim id=admin -claim redmineApiKey="$vncd_api_key" -alg HS256 -sign + -key -)
	export vnctalk_auth
fi


if [ -z "$prosody_rest_secret" ]; then
	prosody_rest_secret=$(openssl rand -hex 40)
	export prosody_rest_secret
fi

if [ -z "$remote_del_token" ]; then
	remote_del_token=$(openssl rand -hex 40)
	export remote_del_token
fi

if [ -z "$solr_admin_pass" ]; then
	solr_admin_pass=$(openssl rand -hex 40)
	export solr_admin_pass
fi

if [ -z "$solr_basicauth" ]; then
	solr_basicauth1=$(echo -n "admin:$solr_admin_pass" | base64 -w 0)
	solr_basicauth=$(echo -n "Basic $solr_basicauth1")
	export solr_basicauth
fi

if [ -z "$vncd_api_key" ]; then
	vncd_api_key=$(openssl rand -hex 20)
	export vncd_api_key
fi

if [ -z "$vncd_groupchat_info_auth_secret" ]; then
	vncd_groupchat_info_auth_secret=$(openssl rand -hex 40)
	export vncd_groupchat_info_auth_secret
fi

if [ -z "$vncdirectory_stat_secret" ]; then
	vncdirectory_stat_secret=$(openssl rand -hex 40)
	export vncdirectory_stat_secret
fi

if [ -z "$xmpp_conference_secret" ]; then
	xmpp_conference_secret=$(openssl rand -hex 40)
	export xmpp_conference_secret
fi


mkdir -p output

envsubst < ./templates/certificates.yaml > ./output/certificates.yaml
envsubst < ./templates/hybridauth.yaml > ./output/hybridauth.yaml
envsubst < ./templates/namespaces.yaml > ./output/namespaces.yaml
envsubst < ./templates/prosody-muc-rest.yaml > ./output/prosody-muc-rest.yaml
envsubst < ./templates/prosody.yaml > ./output/prosody.yaml
envsubst < ./templates/redis.yaml > ./output/redis.yaml
envsubst < ./templates/secrets.yaml > ./output/secrets.yaml
envsubst < ./templates/solr.yaml > ./output/solr.yaml
envsubst < ./templates/vncavatar.yaml > ./output/vncavatar.yaml
envsubst < ./templates/vncdirectory.yaml > ./output/vncdirectory.yaml
envsubst < ./templates/vnc-fileshare.yaml > ./output/vnc-fileshare.yaml
envsubst < ./templates/vnc-idp.yaml > ./output/vnc-idp.yaml
envsubst < ./templates/vncproject.yaml > ./output/vncproject.yaml
envsubst < ./templates/vnctalk.yaml > ./output/vnctalk.yaml
envsubst < ./templates/vnctask.yaml > ./output/vnctask.yaml

envsubst < ./templates/search-admin.yaml > ./output/search-admin.yaml
envsubst < ./templates/search-user.yaml > ./output/search-user.yaml
envsubst < ./templates/talk-indexer.yaml > ./output/talk-indexer.yaml
