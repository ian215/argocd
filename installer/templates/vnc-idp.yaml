apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: vnc-idp
  namespace: argocd
  finalizers:
  - resources-finalizer.argocd.argoproj.io
spec:
  destination:
    server: https://kubernetes.default.svc
    namespace: vnc-idp
  project: default
  source:
    path: charts/vnc-idp
    repoURL: ${repoURL}
    targetRevision: HEAD
    helm:
      parameters:
        - name: nameOverride
          value: "vnc-idp"
      values: |
        imagePullSecrets:
        - name: gcr-json-key
        api:
          enabled: false
        replicaCount: ${minscaling}

        image:
          tag: development-510a4b5e
          pullPolicy: Always
          repository: ${vnc_image_repository}/vnc-localidp

        securityContext:
          privileged: true

        ingress:
          annotations:
            kubernetes.io/ingress.class: "${ingressClass}"
            nginx.ingress.kubernetes.io/proxy-body-size: 10m
          enabled: true
          tls:
          - hosts:
            - "${idp_hostname}"
            secretName: idp-tls
          hosts:
          - host: "${idp_hostname}"
            paths: ["/"]
            service:
              name: vnc-idp
              port:
                number: 80

        ingress_private:
          enabled: false

        autoscaling:
          minReplicas: 1

        env:
          # idp config
          BASE_URL_PATH: 'https://${idp_hostname}/'
          # idp config - integrate vncd registration / self care flow
          LOGOUT_REDIRECT: 0
          LOGOUT_REDIRECT_URL: 'https://${vncdirectory_hostname}'
          REGISTER_ACCOUNT_URL: 'https://${vncdirectory_hostname}/account/register'
          FORGOT_PASSWORD_URL: 'https://${vncdirectory_hostname}/account/lost_password'
          APP_TITLE: '<strong>VNC</strong>lagoon'
          IDP_THEME: "Airbus"
          # idp config - database for sessions and openidconnect
          IDP_DB_DSN: 'pgsql:host=${postgresql_host};port=5432;dbname=${idp_db_name}'
          IDP_DB_USER: '${idp_db_username}'
          IDP_DB_PASS: '${idp_db_password}'
          IDP_STORE_TYPE: 'sql'
          # idp config - basic security and info
          IDP_ADMIN_PASS: '${idp_admin_pass}'
          IDP_SALT: '${idp_salt}'
          IDP_CONTACT_NAME: 'VNC Admins'
          IDP_CONTACT_MAIL: 'admin@${vnclagoon_domain}'
          IDP_COOKIE_DOMAIN: '.${vnclagoon_domain}'
          IDP_USER_DOMAIN: '${vnclagoon_domain}'

          # authsource - pointer to vncd db and JWT
          IDP_VNCDIRECTORY_DB_DSN: 'pgsql:host=${postgresql_host};port=5432;dbname=${vncdirectory_db_name}'
          IDP_VNCDIRECTORY_DB_USER: '${vncdirectory_db_username}'
          IDP_VNCDIRECTORY_DB_PASS: '${vncdirectory_db_password}'
          IDP_VNCDIRECTORY_JWT_SECRET: '${jwt_secret}'

          # metadata - only metadata for added sp is actually set
          VNCDIRECTORY_SAML_ENTITY_ID: 'https://${vncdirectory_hostname}/saml/metadata'
          VNCDIRECTORY_SAML_CALLBACKURL: 'https://${vncdirectory_hostname}/auth/saml/callback'
          VNCDIRECTORY_SAML_LOGOUTURL: 'https://${vncdirectory_hostname}/auth/saml/sls'

          VNCPROJECT_SAML_ENTITY_ID: 'https://${vncproject_hostname}/saml/metadata'
          VNCPROJECT_SAML_CALLBACKURL: 'https://${vncproject_hostname}/auth/saml/callback'
          VNCPROJECT_SAML_LOGOUTURL: 'https://${vncproject_hostname}/auth/saml/sls'
          VNCPROJECT_DCMAP: 'cn=VNCProject,ou=groups,dc=${vncproject_domain_dcparts}'

          VNCTALK_SAML_ENTITY_ID: 'saml-vnctalk-k8s'
          VNCTALK_SAML_CALLBACKURL: 'https://${vnctalk_hostname}/api/login/callback'
          VNCTALK_SAML_LOGOUTURL: 'https://${vnctalk_hostname}/api/logout'

          VNCTASK_SAML_ENTITY_ID: 'saml-task-k8s-dev'
          VNCTASK_SAML_CALLBACKURL: 'https://${vnctask_hostname}/api/login/callback'
          VNCTASK_SAML_LOGOUTURL: 'https://${vnctask_hostname}/api/logout'

  syncPolicy:
    automated:
      prune: true
      selfHeal: true
