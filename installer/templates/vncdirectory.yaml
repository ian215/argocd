apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: vncdirectory
  labels:
    name: vncdirectory
  namespace: argocd
  finalizers:
  - resources-finalizer.argocd.argoproj.io
spec:
  destination:
    server: https://kubernetes.default.svc
    namespace: vncdirectory
  project: default
  source:
    path: charts/vncdirectory
    repoURL: ${repoURL}
    targetRevision: HEAD
    helm:
      parameters:
        - name: nameOverride
          value: "vncdirectory"
      values: |
        imagePullSecrets:
        - name: gcr-json-key
        replicaCount: 1
        nfs:
          enabled: true
          server: "${nfs_server}"
          path: "${vncdirectory_nfs_path}"
        onprem: false
        image:
          repository: ${vnc_image_repository}/vnclagoon
          pullPolicy: Always
          # Overrides the image tag whose default is the chart appVersion.
          tag: development-cdbe0d46

        sidekiq:
          image:
            repository: ${vnc_image_repository}/vnclagoon
            pullPolicy: Always
            # Overrides the image tag whose default is the chart appVersion.
            tag: development-cdbe0d46

        ingress:
          annotations:
            kubernetes.io/ingress.class: "${ingressClass}"
            nginx.ingress.kubernetes.io/proxy-body-size: 100m
          enabled: true
          tls:
          - hosts:
            - "${vncdirectory_hostname}"
            - "wss-${vncdirectory_hostname}"
            secretName: directory-tls
          hosts:
          - host: "${vncdirectory_hostname}"
            paths: ["/"]
            service:
              name: vncdirectory
              port:
                number: 80
          - host: "wss-${vncdirectory_hostname}"
            paths: ["/"]
            service:
              name: vncdirectory-wss
              port:
                number: 81

        ingress_private:
          enabled: false

        autoscaling:
          enabled: false
          minReplicas: 2
          maxReplicas: 2
        resources:
          requests:
            cpu: 800m

        env:
          VNCDIRECTORY_TAGGING_AUTH_SECRET: ${vncdirectory_tag_secret}
          VNCDIRECTORY_POST_STATISTICS_AUTH_SECRET: ${vncdirectory_stat_secret}
          VNCDIRECTORY_SEND_PRODUCT_ENABLE_NOTIFICATIONS: 1
          INTERNAL_CERTIFICATE: "-----BEGIN CERTIFICATE-----\nMIID+DCCAuCgAwIBAgITB24P5i2FS+zwlXhG4UXzvcCPdjANBgkqhkiG9w0BAQ0F\nADCBgDELMAkGA1UEBhMCQ0gxDDAKBgNVBAgMA1p1ZzEUMBIGA1UEBwwLUG9zdHN0\ncmFzc2UxDDAKBgNVBAoMA1ZOQzEUMBIGA1UECwwLRW5naW5lZXJpbmcxKTAnBgNV\nBAMMIFZOQyAtIFZpcnR1YWwgTmV0d29yayBDb25zdWx0IEFHMB4XDTIyMDcwNDEw\nMTYyMFoXDTMyMDcwMzIwMjgyMFowgYAxCzAJBgNVBAYTAkNIMQwwCgYDVQQIDANa\ndWcxFDASBgNVBAcMC1Bvc3RzdHJhc3NlMQwwCgYDVQQKDANWTkMxFDASBgNVBAsM\nC0VuZ2luZWVyaW5nMSkwJwYDVQQDDCBWTkMgLSBWaXJ0dWFsIE5ldHdvcmsgQ29u\nc3VsdCBBRzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMMP/oCrnnys\nLVKepvUp1oH1182YB9wA/j8S3g8lfsCFbmpTNYS49KZnwbEd78LK7zAfAotOfnha\nWy4yU06Gt8em9MGtn8iXwhGG9p/JQ5SQB6CQ28/9zF9+ZlQh5p07E6Mh/4taVtiH\nex4KgwtCRUhEbc6eoU2siIdYcmXP+VHlaZggtWQTrc1oFOC4u8/FPFT7z9FxqI45\n45q31srLdkfmxufBjDvbJZJRePdus6Lo3T01JQ7uJ0niRhkY05ZyW28SXZYu4MF8\nEydzFtAqJdkZxBbq77FymgAIg+wz7GK+/ZrYSrzhy9Pz46Ah0iAkUB+guQFKfLUT\nCCfvRA+mHV0CAwEAAaNpMGcwDwYDVR0TAQH/BAUwAwEB/zAOBgNVHQ8BAf8EBAMC\nAQYwIAYDVR0OAQH/BBYEFDrxcFuTSdPqYBvuX+uET4BHNyTfMCIGA1UdIwEB/wQY\nMBaAFDrxcFuTSdPqYBvuX+uET4BHNyTfMA0GCSqGSIb3DQEBDQUAA4IBAQBU5ral\nz7H+OQgVuqs9rsiSOvC3CYppGuNVecDVDLb3YXgsjm6FZlQ/QrYGnGpqRQrKJoxr\ndTM+d10xMRk91IX+PTmE2XotLTuA/4VlbN6xYfV6kmShSqZyMhWVhTSFXj8GluMa\nLkydIYV8135aZUVtjFMfCJo73kDgzn8MhItJ4rcwg8m0wq2XdArQ+LvOLPzUWB22\n/kLY4Aq+3zWSgu26QjYI2E5ymnIROZfAFGCyA7j5s6pC7ILEO6E8NV/7ubN2v4nY\nqA/3FxSTvrwXm0OekxlPxv76V/VPQewtaX5IG0f1eT5AdndEKPAauS2l8n05MiWh\njB4IV/MJhLtsFcgg\n-----END CERTIFICATE-----"
          INSTANCE_CERTIFICATE: "-----BEGIN CERTIFICATE-----\nMIIDcjCCAlqgAwIBAgIEAJiWgjANBgkqhkiG9w0BAQ0FADCBgDELMAkGA1UEBhMC\nQ0gxDDAKBgNVBAgMA1p1ZzEUMBIGA1UEBwwLUG9zdHN0cmFzc2UxDDAKBgNVBAoM\nA1ZOQzEUMBIGA1UECwwLRW5naW5lZXJpbmcxKTAnBgNVBAMMIFZOQyAtIFZpcnR1\nYWwgTmV0d29yayBDb25zdWx0IEFHMB4XDTIyMDgzMTAwMDAwMFoXDTIzMDgzMTAw\nMDAwMFowTDEJMAcGA1UEBhMAMQkwBwYDVQQIDAAxCTAHBgNVBAcMADETMBEGA1UE\nCgwKQWlyYnVzIEQmUzEJMAcGA1UECwwAMQkwBwYDVQQDDAAwggEiMA0GCSqGSIb3\nDQEBAQUAA4IBDwAwggEKAoIBAQDDOXe4ynuNXrB7rONsiHLq7Cy7+shf9rWLitNg\n4Aojm18CTPqI6rWAKIkRx/2u1TyuoKNqp1RJYffCWFQsl8I6s5Tya8joEOM30TyH\nhi9bWKR/GHvgiiwDmODVMShoZfrfIAOOTP6VZfBx88illIY3DQvvolwd2dttyaEB\nwL7LKnQrsZe9MY8UM7NJIOQUkHU8gN1ysbpaKZSuw84Ci0KrG3Pxjud9YNPvVNd7\n5D4NEi2ufQp3DfZrVgkV0YFtjYgGwZKVKIbG5cGElVGezSNBYlOOgKs3h21fpT+/\nMb694Mz7G2yVZ3OT0Iv8a6b5s/TW1vawP14QTXsxXIiRUinVAgMBAAGjJzAlMAwG\nA1UdEwEB/wQCMAAwFQYDVR0RBA4wDIIKQWlyYnVzIEQmUzANBgkqhkiG9w0BAQ0F\nAAOCAQEAA+g0/GUfpsYg4cS+vCWdQztA7gSLd+NruP6tuSmKh/v7e7cGuD1l045t\nkPwY+NFkCQ1ONuhGPfVMIvuDoC0SNCBY9Hg3ijFhYzyfg4JQfkb046KuNvSe5JnP\nMOnP2mbcMy2JbEvPIa756Cic9M1qLcnUtnad/2BP1l/32WI4ijSoqQlmxN/tJ6ji\nXVLtug7tbKRp8dxqGpi1qMvsZyalrMzurT1S5hhKJ1+QL1ta8+vANcxRJmyw6tQV\nXKRUm7c4NylOo0R5qaqs+bInajeGXrWwznJi5pCgLgFM96fwAPASac5xB9FW88GD\nmD5eERsIL41VfNHXzqElBLvv9PlAdA==\n-----END CERTIFICATE-----\n"
          INSTANCE_ENC_DATA: "ezpkYXRhPT4iXHg4M1x4OTVceEYxXHhGNmlcZVx4QUZceEVDXHhCQVtceEU5\nXHgxQ1x4OEFceEQ4XHhFMlx4QkNceEQxXHg5QkFceEEyUVx4OEJceEUxXHhC\nMFx4OEVceEJFOlx4RkFceEM3XHhDODJceEMzXHhCRXBQR1x4RDdceEYyXHhF\nOVx4Qjc6XHg5REJceDA0clxlI1x4QTRceEEzXHg5OFx4QjVqNFx4RUNceEEz\nXHhEOFx4RDhtIVx4QjlceDgwXHhGMD9ceEM4XHhCMFx4RDZceDk4XHhBOVx4\nOTN4c1x4RjNEOVx4OURceEIxW1x4OTJceDlEMlx4OTFceEI2Ulx4RDJceDgw\nS1x4RjRceDk4P1x4RUUgXHg5QlxuXHgwM1x4ODhVXHg4MFx4MTBRUVx4OTRH\nXHhEQ1x4RjJceDE2LVx4QTkjXHgxNylceDg0XHg4RFx4RDFceEQxL1x4QUJc\neEI5QXpceDhCXHhBNTlXXHRnXlx4RTdceEJBIiwgOmNsaWVudF9rZXk9PiIt\nLS0tLUJFR0lOIFJTQSBQUklWQVRFIEtFWS0tLS0tXG5NSUlFb3dJQkFBS0NB\nUUVBd3psM3VNcDdqVjZ3ZTZ6amJJaHk2dXdzdS9ySVgvYTFpNHJUWU9BS0k1\ndGZBa3o2XG5pT3ExZ0NpSkVjZjlydFU4cnFDamFxZFVTV0gzd2xoVUxKZkNP\nck9VOG12STZCRGpOOUU4aDRZdlcxaWtmeGg3XG40SW9zQTVqZzFURW9hR1g2\nM3lBRGpreitsV1h3Y2ZQSXBaU0dOdzBMNzZKY0hkbmJiY21oQWNDK3l5cDBL\nN0dYXG52VEdQRkRPelNTRGtGSkIxUElEZGNyRzZXaW1VcnNQT0FvdENxeHR6\nOFk3bmZXRFQ3MVRYZStRK0RSSXRybjBLXG5kdzMyYTFZSkZkR0JiWTJJQnNH\nU2xTaUd4dVhCaEpWUm5zMGpRV0pUam9Dck40ZHRYNlUvdnpHK3ZlRE0reHRz\nXG5sV2R6azlDTC9HdW0rYlAwMXRiMnNEOWVFRTE3TVZ5SWtWSXAxUUlEQVFB\nQkFvSUJBSFNJU1Q2MUh5c3o2b1RvXG5ucGlhbW41QW5ETlpiMkpaRWVWSjZR\nTmF1TEtvZS80VGxUbEZZVEl2QUNMdERqRGVlR3FFMlFkQStyOFRDNkdtXG51\nQmdtUG5wcjErMGRDYi84MUtUczhRUzB4Ry95YWI2MERyM3B2SHdwTll3TTIr\nSDFmZHJTY1V6d0NNZUZJaEhqXG5RMUlUWmtwV0wxbkx3TkRReW5xdWNMMXJJ\nSGlFd2VTb2tuVEp3WmMvdXJ6a3p0aWgrazZhTHZKNmtGZkE4citIXG5NaFFm\nVEVHOTZzQUtGcUpybmg5bHBpbnBVUm9KYk85VzdSUDRrVVhuL2x0Q0YyaC9G\nT0dqb25Kc0RKbzZLblBMXG5PZllRQkphUnFnZ2hmNzJjOW83VjBsSjdLYSt5\nbldsRkxiNUdPSE5Od0RhVlJmK2ZIYWpIcFBkcDFLbzlvREdCXG4vRXRQS2NF\nQ2dZRUEvZWpsd014eG9tLzl5NkxKMm5ObkJ0c3E1eWRwUU4xTi9VbitLY1Nu\nMmRrUkpQQ0ZSYkZMXG5VSUJ6cFFrYnBKSmtEeWQzbFp3MUZlT21xTDNsRnJK\nbzdxWXRxYm12SEZRK1hjOXhvQUJiWmFNY0FLRmhnQUVjXG50aFk5MDcwUGFW\nTWVMNkdGOHdEM3VwLzVsR0crQzdrTnlOdDc1REIwQ0hiQnRQRVZ2MVRRZWlN\nQ2dZRUF4TlRrXG4wVEx5ZEZTZ2xiQmsvYlhJZE40M3VBUncrZkdYL1NtTXJD\nQUo4QmN1eExSQ0lOTkxPUVdGOXNVd1NlSDBIc2kvXG45UERNUjBFTjVVdXJs\nb2hSdmxVMnpTME1VamhrK2hNN0RyRWU5SXRqSFF0cENiYVljYSs1RlVKd1Fo\nU2RDNFpoXG5TV1RGT0tEZnZVR0tJbVNIYUNKNjZyZlJpWnhicGJHbjl4MDYz\nNmNDZ1lBWCsxMW4vbWRsUGtxc1p5M0Zra3pTXG4ydlNNYks2R1QrREQ0YTg5\nNXJNcjRsSzZPSFYrQmFibGJaeCtwQU9XWWJWTnNSNmNaT3FtckovYkpFUjFC\nbGplXG40WmRDaHFWQmVidUNSTXVvbnlBQzA0c2JqQ0tYWW9PQnQwd1RxYkhB\nOHZiUTdPS245eGx1NlJrN3FKZTRwN3ZqXG5rc2Q2NEJDMWsvSHIwekdJcHN3\naUhRS0JnUUNuNEV3b2dTTjVSSlNTRnhpTHEzZUU5ZUxmK0NuRGcrTDE4WElo\nXG5mRkg5akVnL1RONzUwRHpnTFczTXd2SWtrYXI4ZG93Q0JUNjFweTZLMnJk\nR2RJNERuL3BWbEk1bHNlT3R4cWtnXG5Kc0VZSWFvdkk0UTYzSm00blA5bXVu\ndTBZSkJ4Ty9QTi9UeVl4OTBNTmRIeTgzcnB2ajdING0rOHhyY0Q0em8yXG44\nWjg5RVFLQmdIZmNQZktSWWx3NnZ0VGVyYUpncEhXaUZRdG91WHkvWU9nSzZ5\nVEw5M2xlSlVZNUdiaGw4a3NWXG5IM2tXVEQzR3crV1lFSkkvWEY2TDh2RlVn\nK2ErQTV3cFV6UnZNMUdJY252MWJPT3NXRTNwbXRQSWdkYi9nMEZrXG5TVGtq\nNEgrUmw3UTFLYm5KcHM2S0I1UWZMUllvdVFoL25WL0lOTjc0MzB3LzRMOTVa\nL1F6XG4tLS0tLUVORCBSU0EgUFJJVkFURSBLRVktLS0tLVxuIiwgOmNpcGhl\ncl9rZXk9PiJceDgxYlx4OTNceDE2eGQ7XHRceEQ3XHhFNmZceDA1XHhCM1x4\nQzR9VGQ7XHhGQnBceDgyV1x4MEZcZSZ6XHg4N1x4OTlceEE4XHhDN1x4QTFc\neDA1XHhGRVBcZUFpSFx4QTRceEY1bVx4Q0VceDg3XHg4NSNceEQ4XHhBOVNc\neDFDXHhDMFx4RTFceEZGRVx4QUVceEE5XHg4MFx4QUJ7XHhEOFx4RjZceDAz\nXHhFMVx4OTNceDk0XHhFMlx4MURceEJGWlx4QjdceEVDXHhCN1x4RTFceEQ1\nXHhCOD5oXHg5OVx4ODZceEQ4XHgxQVx4MTJyXHhEMVx4MDNceEMxXHhEN1x4\nOTJceDA0XHg4Nlx4OTMwXHhCNlx4QjVceERGXHhEMVx4QTdNXHhDRDBceEQy\nXHhENThcdksgKFx4OENceEQ2XHgxM1x4OERZXHhBRFx4QUFceEM3VCtlXHhF\nRkl9XHhBRlx4MTNceDFDXHhEM1x4RDNceEVGXHhDRVx4RkEhXHhGOSZceDdG\nOytceEM3KkpeXHhDOFx4QkVceEMzXHg4Rlx4Q0F2WTlceDlFXHg5MFx4OUJU\nXHhCRHd7XHg5RFx4QkF+XHg4Nlx4RTZceERDTlR2eyhceDhEXHhFMkBrXHhE\nMWpKXHg4RVx4MTNceDk1XHhEN2VceDlGQFx4QjUkRXJceDk4XHhCNVx4RkFc\neDA1XHhFRkVdM1x4MUNceDlBRlx4OTdceDBFR2B1XHhBRCFyXHhDN1x4RTV+\ndjVceEYxXHhEQ1ZceDE1XHhBQVx4OUVceDk4XHg4RVx4OTUxXHg5QVx4QjJc\neDg0XGZceDE3ey1GSmFceEE2XHhEN1x4RTFceEYzXHg4NFx4MTU0XHgxQV9c\neDhGRkovXHg4OFx4RjBFXHhGQ1x4MDJceDgxd2ReXHhFNVx4REFVYlx4Rjkl\nXHg5Q1x4MDIiLCA6Y2lwaGVyX2l2PT4iXHg5NFx4RUNceDlFXHhDNlx4QjU3\nPXFtXHhBMVx4QUZceEVGJlx4QkJceDlCXHhFRFx4ODFXXG5ceEZEXHhDQn1c\neEQ3a1x4OUJceEE1c1x4QTJXJFx4QzdceDFBXHhGNzJXXHhFNyZceDlGIFx4\nQ0NceDBFXHg5NmhIXHg4M1x4Q0F7bVx4OUIjXHhCQlx4QTVJXHg5Q2NceDEx\nXHg5Q1x4QkZVXHgxOFx4MUU7XHhDOVx4ODVceEU5XHg5Mlx4MDJceEQwXHhB\nQjdceEM3XHhDQzpKXHhCNFx4QjRceDkwKF4vXHhBNVx4RTJnXHgxNlx4ODlc\neDlGXGVceEU0XHhGMVx4OTdTXHhDM3JqXHhFRFx4OUM1XHhBRjlwXHhGOVx4\nQTRceEFEXHgwMkUzK1x4OENceEMyXHhGQVx4Q0RQR1x4QjlceEQzXHgxN1x4\nRkVceEEyXHgxRFx4RERceEVFXHgxRlZceDEyXHhGM2NJXHg5Qlx4Q0VceEY5\nXHgxNTJVXHhEQlx4MEVSXHg5Q1x4QkJceEVDXHhDN1x4QUNceEZEaFx4QTJc\neEFGXHg4OVx4ODdceDhDW1x4OERcZnhceDk3KVRceDBGXHhCMFx4QTBFXHhG\nNWVceDE5XHhGMjFhc3Ncclx4RjFceEMzXGJceDkxQlx4MTlYdVx4QUZceEIz\nXHhCQU5ceDg4XHgxN1x4QUZceDE4XHhGNFx4ODddXHgxQVx4RURceEIwXHhG\nRSVceDE5XHhERkZceERDXFxceDk2UDdtelx4QTJceEM2XHhDMFx4REJGLVx4\nQTRceEY5XHgxNlx4QkNceEI0elx4RENceEVGXHhEREhQXHg5Qlx4ODNceEI2\nUjRceEU2MHBceEFDelx4RjdceEY4XHgxM1x4RDZceEQxXHhBQlxlUVx4MTRc\neDFFRiQge0d4XHhERFx4RjhcdFx4QTZceEI0LVx4OThcdFx4MUVceEU0XHg5\nMyJ9\n"
          DB_HOST: ${postgresql_host}
          DB_PORT: 5432
          DB_USER: ${vncdirectory_db_username}
          DB_PASS: ${vncdirectory_db_password}
          SECRET_KEY_BASE: ${vncd_vncp_secretkeybase}
          ADMIN_API_KEY: ${vncd_api_key}
          IDP_THUMBPRINT: FB:29:B9:B5:2C:33:FC:13:0E:BB:36:F5:A4:4B:DA:7A:E1:30:9D:C0
          assertion_consumer_service_url: https://${vncdirectory_hostname}/auth/saml/callback
          issuer: https://${vncdirectory_hostname}/saml/metadata
          single_logout_service_url: https://${vncdirectory_hostname}/auth/saml/sls
          signout_url: https://${idp_hostname}/saml2/idp/SingleLogoutService.php\?ReturnTo\=https://${vncdirectory_hostname}
          idp_sso_target_url: https://${idp_hostname}/saml2/idp/SSOService.php
          idp_slo_target_url: https://${idp_hostname}/saml2/idp/SingleLogoutService.php
          REDIS_HOST: redis-master.redis
          REDIS_PORT: 6379
          SHARE_REDIS_INSTANCE: 1
          REDIS_NAMESPACE: "VNCdirectory"
          SMTP_SERVER: ${smtp_relay}
          VNCDIRECTORY_IS_INTERNAL: 1
          WORKERS_NUM: 5
          RAILS_MAX_THREADS: 50
          FIREBASE_AUTH_KEY: AAAAzg5WLbQ:APA91bHj8HCZ-jdk9r2XQ4B1IG6BcTjQxf-3azMfNd1f3FCDeXynx-uasROO2tKK_FVxaRgDRKKOdqmCeP_Kisj9qzgwCkWyXj319kf2po58eogMtU8o75WsZWRfy-HhHHIia5OuigFE
          FIREBASE_PROJECT_ID: "885003791796"
          FIREBASE_GROUP_KEY_PREFIX: VNCd-DEV
          GROUP_CHAT_INFO_AUTH_SECRET: ${vncd_groupchat_info_auth_secret}
          VNCDIRECTORY_DEFAULT_ADMIN_ID: 1
          #New keys
          XMPP_SERVER_URL: http://api.prosody-muc-rest:9511
          XMPP_SERVER_USERNAME: vnctalk
          XMPP_SERVER_PASSWORD: ${prosody_rest_secret}
          VNCTALK_TRIGGERS_URL: https://${vnctalk_hostname}
          VNCTALK_AUTH: ${vnctalk_auth}
          CUSTOMER_DOMAIN: ${vnclagoon_domain}
          PUBLIC_INSTANCE: 0
          NEW_ORG_DEFAULT_PRODUCTS: vnctalk|channels|vncproject|vnctask|twofa|rfc
          HIDE_EMAIL_FOOTER: 1
          ALTERNATE_EMAIL_HEADER_URL: https://${vncdirectory_hostname}/images/vncdirectory_email_header.png
          VNCPROJECT_BASE_URL: https://${vncproject_hostname}
          VNCPROJECT_ADMIN_API_KEY: ${vncd_api_key}
          # VNCPROJECT_SAAS_BASE_URL: ""
          # VNCPROJECT_SAAS_ADMIN_API_KEY: ""
          # VNCPROJECT_SAAS_SUBDOMAIN_TEMPLATE: ""
          VNCTALK_GOOGLE_PLAY_URL: "https://play.google.com/store/apps/details?id=biz.vnc.vnctalk"
          VNCTALK_APPLE_STORE_URL: https://apps.apple.com/us/app/vnctalk/id1400937435
          VNCTALK_WINDOWS_CLIENT_URL: https://newpackages.vnc.biz/public/VNCtalk.exe
          VNCTALK_MAC_CLIENT_URL: https://newpackages.vnc.biz/public/VNCtalk.dmg
          VNCTALK_LINUX_CLIENT_URL: https://newpackages.vnc.biz/public/VNCtalk.linux.AppImage
          SOLR_INDEXING_ENABLED: 1
          ENABLE_GLOBAL_AVATAR: 1
          AVATAR_SERVER_URL: https://${avatar_hostname}
          AVATAR_SERVER_USERNAME: avatar
          AVATAR_SERVER_PASSWORD: ja8cafei8waiph7IevahGhahnoh21
          JWT_SECRET: "${jwt_secret}"
          DISABLE_JWT_AUTHENTICATION: 0
          TRANSFER_INCIDENT_AUTOMATICALLY: 0
          ASK_QUESTION_TO_CUSTOMER: 0
          SUPPORT_EMAIL: support@{vnclagoon_domain}
          GEOCODE_API_KEY: "AIzaSyD-bavYGZVhFu6_ULHctOLsOHX79eQkzzA"
          EXTERNAL_URL_TALK: https://${vnctalk_hostname}
          EXTERNAL_URL_TASK: https://${vnctask_hostname}
          EXTERNAL_URL_PROJECT: https://${vncproject_hostname}
          EXTERNAL_URL_CHANNELS: https://${vnctalk_hostname}/talk/channels
          SEARCH_ADMIN_URL: "http://searchadmin-solrcloud.solrcloud"
          SEARCH_ADMIN_API_KEY: "eyJhbGciOiJIUzUxMiJ9.eyJhdXRob3JpdGllcyAiOiJST0xFX2FkbWluIiwic3ViIjoiYWRtaW4ifQ.CJWDJTocVbJ4ElTdZgXWhKtQNKIQoZHuH1fHtT7FvNC23a2Cy9r06D1C3ii5Zkgtu_eRzOWdyQe3kgCttZfUmA"
          DISABLE_USER_TO_CONTACT_MAPPING: 1
          VNCTALK_EMISSION_EMAIL: "${vnctalk_admission_email}"
          MANAGE_OWNCLOUD_QUOTA: 0
          DEFAULT_OWNCLOUD_QUOTA: "10485760"
          NOTIFICATION_PROXY_URL: "http://vnctalk-notificationproxy.vnctalk:3898/notify"
          IOM_CHANNELS_ENABLED: 0

        locales:
          en:
            enabled: true
            values:
              vnc_product_vncchannels: "VNCchannels"
              vnc_product_vnctalk: "VNCtalk"
              vnc_product_vncsafe: "VNCcloud"
              vnc_product_vnctask: "VNCtask"
              vnc_product_vncproject: "VNCproject"
              subject_product_information_email: "AIRdirectory account has been enabled\
                \ for %{product_name}"
              body_product_information_email_vnctalk: "Hi %{firstname},<br/><br/>Your\
                \ AIRdirectory account has been enabled for AIRtalk.<br/><br/>URL: %{app_url}<br/>For\
                \ more information, check out our user manual in<br/>- English: <a href='https://en.docs.vnc.biz/vnctalk/usermanual-desktop/'>https://en.docs.vnc.biz/vnctalk/usermanual-desktop/<br/>-\
                \ German: <a href='https://de.docs.vnc.biz/vnctalk/usermanual-desktop/'>https://de.docs.vnc.biz/vnctalk/usermanual-desktop/</a><br/><br/>%{mail_footer}"
              body_product_information_email_vncsafe: "Hi %{firstname},<br/><br/>Your\
                \ AIRdirectory account has been enabled for AIRcloud - our internal file\
                \ sync & share application.<br/>You can access your AIRcloud account via:\
                \ %{app_url}.<br/>For more information, check out our user manual: <a\
                \ href='https://en.docs.vnc.biz/vncsafe/vncsafemanual/'>https://en.docs.vnc.biz/vncsafe/vncsafemanual/</a>\
                \ <br/><br/>%{mail_footer}"
              body_product_information_email_vncproject: "Hi %{firstname},<br/><br/>Your\
                \ AIRdirectory account has been enabled for AIRproject - our internal\
                \ project management, time logging and helpdesk application.<br/><br/>%{mail_footer}"
              body_vncdirectory_account_info_section3b: " "
              body_product_information_email_vnctask: "Hi %{firstname},<br/><br/>Your\
                \ AIRdirectory account has been enabled for AIRtask.<br/><br/>URL: %{app_url}<br/>For\
                \ more information, check out our user manual in<br/>- English: <a href='https://en.docs.vnc.biz/VNCtask/taskusermanualdesktop/'>https://en.docs.vnc.biz/VNCtask/taskusermanualdesktop/<br/>-\
                \ German: <a href='https://de.docs.vnc.biz/VNCtask/webclient/'>https://de.docs.vnc.biz/VNCtask/webclient/</a><br/><br/>%{mail_footer}"
              product_desc_calendar: "Click here to open the owncloud webclient in a new\
                \ tab!"
              mail_subject_lost_password: "Lost your password?"
              mail_body_lost_password: |-
                Hello %{username},<br/><br/>
                you triggered the "Lost Password" functionality for your account on %{home_url}.<br/>
                Please click the following link in order to set a new password for your account:<br/><br/>
                %{reset_link}<br/><br/>
                Username: %{login}
              text_meeting_greetings: "\"%{moderator_name}\" has invited you to join the\
                \ video conference \"%{meeting_title}\" on %{meeting_time}.<br/>If you\
                \ want to join the conference please click the button below at given time\
                \ (5 minutes before)<br/> %{password_line}"
          de:
            enabled: true
            values:
              vnc_product_vncchannels: "VNCchannels"
              vnc_product_vnctalk: "VNCtalk"
              vnc_product_vncsafe: "VNCsafe"
              vnc_product_vnctask: "VNCtask"
              vnc_product_vncproject: "VNCproject"
              subject_product_information_email: "Der AIRdirectory-Account für %{product_name}\
                \ wurde aktiviert"
              body_product_information_email_vnctalk: "Hallo %{firstname},<br/><br/>Ihr\
                \ AIRdirectory-Account für AIRtalk wurde aktiviert.<br/><br/>URL: %{app_url}<br/>Weitere\
                \ Informationen finden Sie in unserem Benutzerhandbuch auf<br/>- Englisch:\
                \ <a href='https://en.docs.vnc.biz/vnctalk/usermanual-desktop/'>https://en.docs.vnc.biz/vnctalk/usermanual-desktop/<br/>-\
                \ Deutsch: <a href='https://de.docs.vnc.biz/vnctalk/usermanual-desktop/'>https://de.docs.vnc.biz/vnctalk/usermanual-desktop/</a><br/><br/>%{mail_footer}"
              body_product_information_email_vncsafe: "Hallo %{firstname},<br/><br/>Ihr\
                \ AIRdirectory-Account für AIRsafe - unsere interne File Sync&Share-Anwendung\
                \ - wurde aktiviert.<br/>Sie können auf Ihren AIRsafe-Account über folgende\
                \ URL zugreifen: %{app_url}.<br/>Mehr Informationen finden Sie im Benutzerhandbuch:\
                \ <a href='https://en.docs.vnc.biz/vncsafe/vncsafemanual/'>https://en.docs.vnc.biz/vncsafe/vncsafemanual/</a>\
                \ <br/><br/>%{mail_footer}"
              body_product_information_email_vnctask: "Hallo %{firstname},<br/><br/> Ihr\
                \ VNCdirectory-Account für VNCtask wurde aktiviert.<br/><br/>URL: %{app_url}<br/>Weitere\
                \ Informationen finden Sie in unserem Benutzerhandbuch auf<br/>- Englisch:\
                \ <a href='https://en.docs.vnc.biz/VNCtask/taskusermanualdesktop/'>https://en.docs.vnc.biz/VNCtask/taskusermanualdesktop/<br/>-\
                \ Deutsch: <a href='https://de.docs.vnc.biz/VNCtask/webclient/'>https://de.docs.vnc.biz/VNCtask/webclient/</a><br/><br/>%{mail_footer}"
              product_desc_calendar: "Klicke hier, um den ownCloud Webclient in einem\
                \ neuen Tab zu öffnen!"
              mail_subject_lost_password: "Passwort-vergessen?"
              mail_body_lost_password: |-
                Hallo %{username},<br/><br/>
                Sie haben eine Passwortänderung für Ihren Account auf %{home_url} angefragt.<br/>
                Klicken Sie auf den folgenden Link, um ein neues Passwort für Ihren Account zu setzen:<br/><br/>
                %{reset_link}<br/><br/>
                Benutzername: %{login}
              body_product_information_email_vncproject: "Hallo %{firstname},<br/><br/>\
                \ Ihr VNCdirectory-Account für VNCproject - unsere interne Projektmanagement-,\
                \ Zeiterfassungs- und Helpdesk-Anwendung – wurde aktiviert.<br/><br/>%{mail_footer}"
              body_vncdirectory_account_info_section3b: " "
              text_meeting_greetings: "\"%{moderator_name}\" hat Sie zur Videokonferenz\
                \ \"%{meeting_title}\" am %{meeting_time} eingeladen.<br/>Wenn Sie teilnehmen\
                \ möchten, treten Sie bitte 5 Minuten vor Beginn bei:<br/>%{password_line}"

  syncPolicy:
    automated:
      prune: true
      selfHeal: true
