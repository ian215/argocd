apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: vnctalk
  namespace: argocd
  finalizers:
  - resources-finalizer.argocd.argoproj.io
spec:
  destination:
    server: https://kubernetes.default.svc
    namespace: vnctalk
  project: default
  source:
    path: charts/vnctalk
    repoURL: ${repoURL}
    targetRevision: HEAD
    helm:
      parameters:
        - name: nameOverride
          value: "vnctalk"
      values: |
        imagePullSecrets:
        - name: gcr-json-key
        podAntiAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
          - labelSelector:
              matchExpressions:
              - key: app
                operator: In
                values:
                - talk
            topologyKey: "kubernetes.io/hostname"
        domain: ${vnclagoon_domain}
        xmpp_domain: xmpp.${vnclagoon_domain}
        hybridAuthURL: 'http://api.hybridauth:80/'
        uxfweb:
          imagePullSecrets:
          - name: gcr-json-key
          replicaCount: ${minscaling}
          image:
            pullPolicy: Always
            repository: ${vnc_image_repository}/vnctalk-uxf
            tag: development-bcb5818b
          autoscaling:
            enabled: false
            minReplicas: 1
            maxReplicas: 1
            targetCPUUtilizationPercentage: 80
          url: ${vnctalk_hostname}
          dist: "dist"
          domain: ${vnclagoon_domain}
          htpasswd:
          serverName:
          fcm_api_key: "AAAAzg5WLbQ:APA91bHj8HCZ-jdk9r2XQ4B1IG6BcTjQxf-3azMfNd1f3FCDeXynx-uasROO2tKK_FVxaRgDRKKOdqmCeP_Kisj9qzgwCkWyXj319kf2po58eogMtU8o75WsZWRfy-HhHHIia5OuigFE"
          fcm_api_url: "http://vnctalk-notificationproxy.vnctalk:3898/notify"
          del_api_url: "http://vnctalk-notificationproxy.vnctalk:3898/delete"


        avatar:
          url: "http://${avatar_hostname}/avatarupload/"
          user: "avatar"
          password: "ja8cafei8waiph7IevahGhahnoh21"
        conference:
          url: "conference.${vnclagoon_domain}"
          secret: "${xmpp_conference_secret}"
        prosody:
          db:
            host: "${postgresql_host}"
            port: 5432
            name: "${prosody_db_name}"
            user: "${prosody_db_user}"
            pass: "${prosody_db_pass}"
        uxfapi:
          image:
            repository: ${vnc_image_repository}/vnctalk-uxf
            tag: development-bcb5818b
            pullPolicy: Always
          env:
            brandingFirstName: "VNC"
            brandingLastName: "talk"
            serviceDeskLink: "https://${vncproject_hostname}/helpdesk/incidents/new"
            manualLinkDe: "https://de.docs.vnc.biz/vnctalk/usermanual-desktop/"
            helpLinkDe: ""
            faqLinkDe: "https://de.docs.vnc.biz/vnctalk/faq/"
            manualLinkEn: "https://devboard.vnc.biz"
            helpLinkEn: ""
            faqLinkEn: "https://de.docs.vnc.biz/vnctalk/faq/"
            domain: "${vnclagoon_domain}"
            xmppWsUrl: "wss://${xmpp_hostname}/xmpp-websocket"
            boshURL: "https://${xmpp_hostname}/http-bind"
            avatarServiceUrl: "https://${avatar_hostname}"
            avatarUsername: "avatar"
            avatarPassword: "ja8cafei8waiph7IevahGhahnoh21"
            solrPath: "http://solrcloud.solrcloud:8983/solr"
            prosodyMucRESTUrl: "http://api.prosody-muc-rest:9511/"
            streamURL: "https://stream.videobridges.vnclagoon.com/368931b88e082af40df10a33349c07f1/"
            fileShareService: "https://${fileshare_hostname}"
            xmppToken: "${jwt_secret}"
            contactsApiUrl: "http://vncdirectory.vncdirectory"
            contactsApiKey: "${vncd_api_key}"
            contactSearchApi: "http://searchuser.solrcloud:80/suap/api/v1/contact/search"
            contactSearchApiKey: "eyJhbGciOiJIUzUxMiJ9.eyJhdXRob3JpdGllcyAiOiJST0xFX2FkbWluIiwic3ViIjoiYWRtaW4yIn0.WV2arOTGsMTNPwf6GIvU-nPjHbYddgu_zk5DfbPumq6z2m3SXwzI-2YbWDoIZpiOi-HqwjUDUB41ulmFntoHEg"
            baseSearchApiV1: "http://searchuser.solrcloud:80/suap/api/v1/"
            epURL: "https://${etherpad_hostname}/"
            epVersion: "2020102201"
            epCookieDomain: "${vnclagoon_domain}"
            limitSearchToOrg: "false"
            limitSearchToDept: "true"
            channelWsUrl: wss://wss-${vncdirectory_hostname}/cable
            vncdDisableUserToContactMapping: false
            bucketName: "vnc-conference-recordings" # need to add to config
            signalSecret: "none"
            SAML_PATH: "/api/login/callback"
            SAML_entryPoint: "https://${idp_hostname}/saml2/idp/SSOService.php"
            SAML_logoutUrl: "https://${idp_hostname}/saml2/idp/SingleLogoutService.php"
            SAML_issuer: "saml-vnctalk-k8s"
            prosodyDbHost: "${postgresql_host}"
            prosodyDbPort: "5432"
            prosodyDbName: "${prosody_db_name}"
            prosodyDbUser: "${prosody_db_user}"
            prosodyDbPass: "${prosody_db_pass}"
            VncDirectoryDbHost: "${postgresql_host}"
            VncDirectoryDbPort: "5432"
            VncDirectoryDbName: "${vncdirectory_db_name}"
            VncDirectoryDbUser: "${vncdirectory_db_username}"
            VncDirectoryDbPass: "${vncdirectory_db_password}"
            federatedAppsVNCtask: "https://${vnctask_hostname}/api/login"
            federatedAppsVNCtalk: "https://${vnctalk_hostname}/"
            redmineURL: "https://${vncproject_hostname}"
            publicServerUrl: "https://${vnctalk_hostname}"
            hybridAuthURL: 'http://api.hybridauth:80/'
            redisHost: 'redis-master.redis'
            redisType: 'standalone'
            prosodyRESTuser: "vnctalk"
            prosodyRESTUrl: "http://http.prosody:5280/rest"
            prosodyMucUrl: "http://api.prosody-muc-rest:9511"
            xmppRestViaService: true
            prosodyRESTsecret: "${prosody_rest_secret}"
            epApiKey: ${etherpad_api_key}
            whiteboardSecret: "not_available"
            jitsiMeetURL: "not_available"
            jitsiURL: "not_available"
            requireJitsiAuth: true
            commanderSolrPath: "http://solrcloud.solrcloud:8983/solr"
            solrAuth: "${solr_basicauth}"
            vncDirectoryPublicUrl: "https://${vncdirectory_hostname}"
            enabledOwnCloud: false
            enable2fa: true
            useVNCdirectoryAuth: true
            signalAPI: "https://${vncdirectory_hostname}"
            SENTINEL_HOST1: redis-standalone-node-0.redis-standalone-headless.redis.svc.cluster.local # only used with redis cluster
            SENTINEL_HOST2: redis-standalone-node-1.redis-standalone-headless.redis.svc.cluster.local
            SENTINEL_HOST3: redis-standalone-node-2.redis-standalone-headless.redis.svc.cluster.local
            redisMasterSetName: 'mymaster'
            GROUP_CHAT_INFO_AUTH_SECRET: ${vncd_groupchat_info_auth_secret}
            tagSecret: "${vncdirectory_tag_secret}"
            # onlyOfficeApiUrl: "https://onlyoffice.${vnclagoon_domain}/web-apps/apps/api/documents/api.js"
            # ownCloudURL: "https://owncloud.${vnclagoon_domain}/"
            vncDirectoryStatSecret: "${vncdirectory_stat_secret}"

        etherpad:
          imagePullSecrets:
          - name: gcr-json-key
          image:
            repository: ${vnc_image_repository}/vnctalk-etherpad
            tag: development-77b35906
          autoscaling:
            enabled: false
            minReplicas: 1
            maxReplicas: 1
          env:
            redmineURL: "https://${vncproject_hostname}"
          apikey: ${etherpad_api_key}
          db:
            host: "${postgresql_host}"
            port: "5432"
            name: "${etherpad_db_name}"
            user: "${etherpad_db_user}"
            password: "${etherpad_db_pass}"
            charset: "utf8mb4"
            type: pgsql
        whiteboard:
          enabled: false
          autoscaling:
            enabled: false
            minReplicas: 1
            maxReplicas: 1
        notificationproxy:
          replicaCount: 1
          imagePullSecrets:
          - name: gcr-json-key
          image:
            repository: ${vnc_image_repository}/vnctalk-notificationproxy
            tag: development-e3489790
          autoscaling:
            enabled: false
            minReplicas: 1
            maxReplicas: 1
            targetCPUUtilizationPercentage: 80
          service:
            type: NodePort
          env:
            notificationServicePort: 3898
            remoteDelToken: "${remote_del_token}"
            remoteDelUrl: "http://delete-file.vnc-fileshare:3898/remote-delete"
            useRedis: true
            redisType: "standalone"
            redisHost: "redis-master.redis"
            redisPort: 6379
            notificationProxyBatchingDelayMSec: 2000
            notificationProxyRedisType: "standalone"
            notificationProxyRedisHost: "redis-master.redis"
            notificationProxyRedisPort: 6379
            notificationProxyRemoteDelToken: "${remote_del_token}"
            notificationProxyRemoteDelUrl: "http://delete-file.vnc-fileshare:3898/remote-delete"
            notificationProxyUseRedis: true
            notificationProxyDbHost: "${postgresql_host}"
            notificationProxyDbPort: 5432
            notificationProxyDbName: "${prosody_db_name}"
            notificationProxyDbUser: "${prosody_db_user}"
            notificationProxyDbPass: "${prosody_db_pass}"
            syncPushInterval: 900

        ingress:
          annotations:
            kubernetes.io/ingress.class: "${ingressClass}"
            nginx.ingress.kubernetes.io/proxy-body-size: 100m
          enabled: true
          host: "${vnctalk_hostname}"
          hosts:
          - host: "${vnctalk_hostname}"
            paths: ["/api/"]
            service:
              port:
                number: 3000
              name: vnctalk-api
          - host: "${vnctalk_hostname}"
            paths: ["/"]
            service:
              port:
                number: 80
              name: vnctalk-web
          tls:
          - hosts:
            - "${vnctalk_hostname}"
            secretName: talk-tls
        ingress_whiteboard:
          enabled: false
        ingress_etherpad:
          annotations:
            kubernetes.io/ingress.class: "${ingressClass}"
            nginx.ingress.kubernetes.io/proxy-body-size: 100m
          enabled: true
          host: "${etherpad_hostname}"
          hosts:
          - host: "${etherpad_hostname}"
            paths: ["/"]
            service:
              port:
                number: 9002
              name: vnctalk-etherpad
          tls:
          - hosts:
            - "${etherpad_hostname}"
            secretName: etherpad-tls
        ingress_notificationproxy:
          enabled: false

        ingress_additional_host:
          enabled: false
        onprem: false

  syncPolicy:
    automated:
      prune: true
      selfHeal: true
